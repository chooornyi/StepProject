(function($) {
    $(function() {
        $('ul.ourServices__caption').on('click', 'li:not(.active)', function() {
            $(this)
                .addClass('active').siblings().removeClass('active')
                .closest('div.ourServices').find('div.ourServices__content').removeClass('active').eq($(this).index()).addClass('active');
        });
    });
})(jQuery);